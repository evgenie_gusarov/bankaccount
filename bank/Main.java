package bank;

public class Main {
    public static void main(String[] args) {

        BankAccount account = new BankAccount();
        RefillOfBalance refillOfBalance = new RefillOfBalance(account);
        GettingOfBalance gettingOfBalance = new GettingOfBalance(account);
        refillOfBalance.start();
        gettingOfBalance.start();
    }
}
