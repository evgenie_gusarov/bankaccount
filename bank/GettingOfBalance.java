package bank;

public class GettingOfBalance extends Thread {
    private final BankAccount account;

    GettingOfBalance(BankAccount account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            synchronized (account) {
                while (account.getBalance() < 2_000) {
                    try {
                        System.out.printf("Ошибка. Для списания %d на балансе недостаточно средств. Необходимо пополнить баланс. \n", 2_000);
                        account.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (account.getBalance() > 0) {
                    account.gettingOfMoney(2_000);
                    System.out.printf("Снятие средств со счёта в размере %d . Баланс: %s \n", 2_000, account.getBalance());
                }
            }
        }
    }
}
